<?php

create table customers (
        id int primary key auto_increment,
        name varchar(255),
        email varchar(255));
        
create table orders (
        id int primary key auto_increment,
        amount int,
        customer_id int);

alter table orders add foreign key (customer_id) references customers(id);
        

?>